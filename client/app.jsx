import "./styles/base.scss";
import "babel-polyfill";
import React, { Component } from "react";
import { render } from "react-dom";
import { Router, Route, IndexRoute, broswerHistory } from "react-router";

import {debounce} from "./utils/debounce";
import { MOBILE_BREAK_POINT, RESIZE_DELAY } from './constants'
import appRoutes from "./routes";

import LoadingOverlay from "./components/common/loadingOverlay";
import MainNav from "./components/common/nav/index";

export default class App extends Component {
  constructor(props) {
    super(props);
    this._handleResize = debounce(this._handleResize.bind(this), RESIZE_DELAY);
    this.state = {
      isMobile: false
    }
  }
  componentDidMount() {
    this._handleResize();
    window.addEventListener('resize', this._handleResize);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this._handleResize);
  }
  _handleResize() {
    let isMobileScreen = window.innerWidth <= MOBILE_BREAK_POINT;
    isMobileScreen !== this.state.isMobile && this.setState({isMobile: isMobileScreen});
  }
  render() {
    const {
      props:{children, history},
      state:{isMobile}
    } = this;

    return (
      <div className="appContainer">
        <MainNav history = {history}/>
        {children && React.cloneElement(children, {isMobile: isMobile})}
        <LoadingOverlay/>
      </div>
    );
  }
}

render(
  <Router>
    <Route path="/" component={App}>
      {appRoutes}
    </Route>
  </Router>, document.getElementById('js-content'));
