import React, { Component } from "react";

export default class NavContent extends Component {
  render() {
    const {
      showBackBtn,
      previousRoute,
      pageTitle,
      showHomeBtn
    } = this.props;

    return (
      <div className = "navInnerContainter">
        <div className="navCol navBkBtnContainer">
          {showBackBtn && <BackBtn {...this.props}/>}
          {showHomeBtn && <HomeBtn {...this.props}/>}
        </div>
        <div className="navCol">
          {pageTitle ? <PageTitle {...this.props}/> : null}
        </div>
        <div className="navCol"></div>
      </div>
    );
  }
}

const BackBtn = (props) => (
  <button
    id="bkBtn"
    className = "btn btn_default"
    onClick={()=>{props.history.goBack()}}>
    Back
  </button>
);

const HomeBtn = (props) => (
  <button
    id="bkBtn"
    className = "btn btn_default"
    onClick={()=>{props.history.pushState(null, "/")}}>
    Home
  </button>
);

const PageTitle = (props) => (
  <div id="pageTitle">
    {props.pageTitle}
  </div>
);
