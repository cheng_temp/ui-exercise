import { fetchTransacHist, fetchTransacDetails, fetchStaticTransacHist } from './apiController';

export function apiRoutes(appRouter) {
	appRouter.get('/transac-hist/:accountId', fetchTransacHist);
	appRouter.get('/transac-hist-static/:accountId', fetchStaticTransacHist);
  appRouter.get('/transac-details/:transId', fetchTransacDetails);
}
