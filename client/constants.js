export const MOBILE_BREAK_POINT = 768;
export const RESIZE_DELAY = 300;

export const CURRENCY_MAP = {
  USD: {LOC:'en-US', SYB:'$'},
  EUR: {LOC:'de-DE', SYB:'€'},
  JPY: {LOC:'ja-JP', SYB:'¥'}
}

export const PAYMENT_TYPES = {
  FRIENDS_FAMILY: 'FRIENDS_FAMILY',
  GOODS_SERVICES: 'GOODS_SERVICES'
}
