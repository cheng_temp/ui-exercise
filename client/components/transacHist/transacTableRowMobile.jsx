import React, { Component } from "react";
import moment from 'moment';

/**
 * TEMP. Todo: Rewrite new hisotry table comp for mobile
 */

export default class TransacTableRowMobile extends Component {
  constructor(props){
    super(props);
  }
  render() {
    const {
      recipientEmail,
      transacDate,
      transacType,
      transacAmount
    } = this.props.data;

    return (
      <div className = "row" style ={{paddingLeft: '30px'}}>
        <table style ={{width: '100%'}}>
          <tr style ={{height: '70px'}}>
            <td style ={{width: '15%'}}>
              Date: <br/> {moment(transacDate).format('MM/DD/YY')}
            </td>
            <td style ={{width: '40%'}}>
              To:<br/> {recipientEmail}
            </td>
            <td style ={{width: '25%'}}>
              For: <br/> {transacType}
            </td>
            <td style ={{width: '20%'}}>
              <p className="label label-primary"
                style ={{fontSize: '1.05em'}}>
                {transacAmount}
              </p>
            </td>
          </tr>
        </table>
      </div>
    );
  }
}
