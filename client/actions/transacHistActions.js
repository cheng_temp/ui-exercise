import Reflux from 'reflux';
import { loadTransacHist } from '../api/transacHist';

const transacHistActions = Reflux.createActions({
  'loadHist' : {asyncResult: true}
});

transacHistActions.loadHist.listen((accountId = '0', limit, offSet) => {
  loadTransacHist(accountId, limit, offSet).then((data) => {
    transacHistActions.loadHist.completed(data);
  }, (failData) => {
    transacHistActions.loadHist.failed(data);
  })
})

export default transacHistActions;
