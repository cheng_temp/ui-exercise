import React, { Component } from "react";

import Btn from "./btn.jsx";

class BtnGroup extends Component {
  constructor(props) {
    super(props);
  }
  _computeColClass(count){
    return `col-md-${Math.floor(12 / count)}`;
  }
  render() {
    const {
      count,
      children,
      handleChange,
      custGrpClassName
    } = this.props;

    return (
      <div className = {`${custGrpClassName || ''}`}>
        {children.map((child)=>(
          React.cloneElement(child, {
            handleChange: handleChange,
            colClassName: this._computeColClass(count)
          })
        ))}
      </div>
    );
  }
}

BtnGroup.Btn = Btn;
export default BtnGroup;
