export function debounce(func, wait = 300){
  let timeout = null;
  return (...arg) => {
    const callFunc = () => {
      timeout = null;
      func.call(this, ...arg);
    }
    !!timeout && clearTimeout(timeout);
    timeout = setTimeout(callFunc, wait);
  }
}
