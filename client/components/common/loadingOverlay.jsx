import React, { Component } from "react";

import appStore from '../../stores/appStore';

export default class LoadingOverlay extends Component {
  constructor(props){
    super(props);
    this.state = appStore.getDefaultData();
    this._onStateChange = this._onStateChange.bind(this)
  }
  componentDidMount() {
    this.unsubscribe = appStore.listen(this._onStateChange);
  }
  componentWillUnmount() {
    this.unsubscribe();
  }
  _onStateChange(data) {
    this.setState(data);
  }
  render() {
    const {
      isLoading,
      bgDimmed
    } = this.state;
    if(!isLoading) return <span></span>
    let placeHolderArray = Array.from(new Array(12));
    return (
      <div className = {`loadingOverlay ${bgDimmed ? 'bgDimmed' : ''}`}>
        <div className="sk-fading-circle">
          {placeHolderArray.map((val, index)=>{
            return <div className={`sk-circle${++index} sk-circle`}></div>
          })}
        </div>
      </div>
    );
  }
}
