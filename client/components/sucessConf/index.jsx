import React, { Component } from "react";

import appActions from "../../actions/appActions";
import SendOrViewBtns from "../common/sendOrViewBtns/index"


export default class SucessConf extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    appActions.updateSpinnerState(false, false);
  }
  render() {
    return (
      <div className = "SucessConfComp">
        <h2> Your payment has been successfully sent </h2>
        <SendOrViewBtns
          handleRouteChange = {(path)=>{
            this.props.history.pushState(null, path);
          }}/>
      </div>
    );
  }
}
