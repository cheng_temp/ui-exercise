import Reflux from 'reflux';

const appActions = Reflux.createActions({
  'updateSpinnerState' : {},
  'routeChanged' : {}
});

export default appActions;
