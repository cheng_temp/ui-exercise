import React, { Component } from "react";
import moment from 'moment'

import { PAYMENT_TYPES, CURRENCY_MAP } from '../../constants'


export default class TransacTableRow extends Component {
  constructor(props){
    super(props);
  }
  render() {
    let {data} = this.props;
    return (
      <div className = "transacHistTableRow">
        <RecipientCell {...data}/>
        <TransacDateCell {...data}/>
        <TransacTypeCell {...data}/>
        <TransacAmountCell {...data}/>
      </div>
    );
  }
}


const RecipientCell = (props) => (
  <TransacTableCell width = {40}>
    <div className="col-md-3 col-sm-4">
      <img className="img-round"
        src={props.recipientAvatar} />
    </div>
    <div className="col-md-9 col-sm-8 recipientEmail">
      {props.recipientEmail}
    </div>
  </TransacTableCell>
);

const TransacDateCell = (props) => (
  <TransacTableCell width = {15}>
    {moment(props.transacDate).format('MM/DD/YYYY')}
  </TransacTableCell>
);


const TransacTypeCell = (props) => (
  <TransacTableCell width = {20}>
    {props.transacType === PAYMENT_TYPES.FRIENDS_FAMILY ?
      <p>Friends and family</p> :
      <p>Goods and services</p>
    }
  </TransacTableCell>
);

const TransacAmountCell = (props) => {
  const LOCALE = CURRENCY_MAP[props.currrencyType].LOC;
  const currencySyb = CURRENCY_MAP[props.currrencyType].SYB;
  const amountNum = new Number (props.transacAmount);
  const amount = amountNum.toLocaleString(LOCALE, { minimumFractionDigits: 2 });
  return(
    <TransacTableCell width = {25}>
      <p className="label label-primary"
        id="transacAmountCell">
        {`${currencySyb} ${amount}`}
      </p>
    </TransacTableCell>
  )
}

class TransacTableCell extends Component {
  render() {
    let {children, width} = this.props;
    return (
      <div className="transacTableCell"
        style={{width: `${width}%`}}>
        {children}
      </div>
    );
  }
}
