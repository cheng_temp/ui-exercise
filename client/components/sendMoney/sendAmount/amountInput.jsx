import React, { Component } from "react";

import {CURRENCY_MAP} from '../../../constants'

export default class AmountInput extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      handleAmountChange,
      currency,
      amountError,
      sendAmount
    } = this.props;


    let currencySyb = CURRENCY_MAP[currency].SYB;

    return (
      <div style={{width:'100%'}}>
        <div className = "amountInputComp">
          <input
            type="tel"
            dir="ltr"
            id="sendAmount"
            autocomplete="off"
            placeholder={`${currencySyb}0.00`}
          	value={`${sendAmount ? currencySyb : ''}${sendAmount || ''}`}
            onChange={handleAmountChange}
            ref={node => {this.sendAmount = node}}/>
        </div>
        <div className ="formError"> {amountError ? <p>{amountError}</p> : null}</div>
      </div>
    );
  }
}
