export function handleInfitScroll(callback){
  const totalScrolled = window.scrollY;
  const docHeight = document.documentElement.scrollHeight;
  const windowHeight =  window.innerHeight;
  if(window.scrollY === (docHeight - windowHeight)){
    callback.call();
  }
}
