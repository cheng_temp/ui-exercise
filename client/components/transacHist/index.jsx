import React, { Component } from "react";

import transacHistActions from '../../actions/transacHistActions';
import appActions from '../../actions/appActions';
import transacHistStore from '../../stores/transacHistStore';
import { debounce } from "../../utils/debounce";
import { handleInfitScroll } from '../../utils/infinitScroll';
import { SCROLL_DELAY } from '../../constants'
import TransacTableHeader from './transacTableHeader';
import TransacTable from './transacTable';

export default class TransacHistory extends Component {
  constructor(props){
    super(props);
    this.state = transacHistStore.getDefaultData();
    this._handleScroll = debounce(this._handleScroll.bind(this), SCROLL_DELAY);
    this._onStateChange = this._onStateChange.bind(this)
  }
  componentDidMount() {
    this.unsubscribe = transacHistStore.listen(this._onStateChange);
    transacHistActions.loadHist(null, this.state.limit, this.state.offset);
    let {documentElement:{addEventListener}} = document;
    addEventListener('scroll', this._handleScroll);
  }
  componentWillUnmount() {
    this.unsubscribe();
    let {documentElement:{removeEventListener}} = document;
    removeEventListener('scroll', this._handleScroll);
  }
  _onStateChange(data) {
    this.setState(data);
  }
  _handleScroll() {
    if(this.state.isInfinitLoading){return};
    let {limit, offset} = this.state;
    handleInfitScroll(()=>{
      transacHistActions.loadHist(null, limit, offset);
    });
  }
  render() {
    const {
      transactions,
      isInitLoading,
      isInfinitLoading,
      transactionColNames
    } = this.state;

    let showLoadingSpinner = isInitLoading || isInfinitLoading;
    appActions.updateSpinnerState(showLoadingSpinner, false);

    return (
      <div className = "transacHistPage">
        <TransacTable
          transactions={transactions}
          isMobile={this.props.isMobile}
          isInitLoading = {isInitLoading}
          isInfinitLoading = {isInfinitLoading}/>
      </div>
    );
  }
}
