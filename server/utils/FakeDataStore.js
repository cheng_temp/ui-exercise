import faker from 'faker';

class FakeDataStore {
  constructor(recordNumber = 500){
    this.recordNumber = recordNumber;
    this.storage = [];
    this.generateRecords();
  }
  generateRecords(){
    for (let i = 1; i <= this.recordNumber; i++){
      this.storage.push(this.generateSingleRecord(i));
    }
  }
  generateSingleRecord(index){
    const transacTypes = ["FRIENDS_FAMILY", "GOODS_SERVICES"];
    const currencyTypes = ["USD", "EUR", "JPY"];

    let randomTransacIndex = Math.round(Math.random()*1);
    let randomCurrencyIndex = Math.round(Math.random()*2);

    return {
      id: index,
      recipientAvatar: faker.image.avatar(),
      recipientEmail: faker.internet.email(),
      transacDate: faker.date.past(),
      transacType: transacTypes[randomTransacIndex],
      transacAmount: parseFloat(Math.random()*2000).toFixed(2),
      currrencyType: currencyTypes[randomCurrencyIndex]
    }
  }
  getRecordsByRange(start, limit){
    if(start > this.recordNumber){return false;}
    let endIndex = parseInt(start) + parseInt(limit);
    return this.storage.slice(start, endIndex);
  }
  getSingleRecord(index){
    if(start > this.recordNumber){return false;}
    return this.storage[index];
  }
}

export default FakeDataStore;
