import React, { Component } from "react";

export default class CurrencySel extends Component {
  render() {
    const {
      currency,
      avilCurrency,
      handleCurrencyChange,
      amountError
    } = this.props;

    return (
      <div className="currencySelectComp">
        <select id="currencySelect"
          onChange={handleCurrencyChange}>
          {avilCurrency.map(val => {
            return currency === val ?
              <option selected="selected" value={val}>
                {val}
              </option> :
              <option value={val}>
                {val}
              </option>
          })}
        </select>
      </div>
    );
  }
}
