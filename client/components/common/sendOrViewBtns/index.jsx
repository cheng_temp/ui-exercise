import React, { Component } from "react";

import BtnGroup from "../btnGroup/index";

export default class SendOrViewBtns extends Component {
  _handleRouteChange(url){
    this.props.history.history.pushState(null, url);
  }
  render() {
    const {handleRouteChange} = this.props;
    return (
      <BtnGroup
        handleChange = {handleRouteChange}
        count = {2}>
        <BtnGroup.Btn
          custBtnClassName = "sendOrViewBtn"
          iconName = "credit-card"
          displayText = "Send money"
          context = "send-money" />
        <BtnGroup.Btn
          custBtnClassName = "sendOrViewBtn"
          iconName = "history"
          displayText = "View Transaction History"
          context = "transaction-history"/>
      </BtnGroup>
    );

  }
}
