import React, { Component } from "react";
import { Route, IndexRoute } from "react-router";

import appAction from './actions/appActions';
import App from './app.jsx';
import Home from './components/home/index';
import SendMoney from './components/sendMoney/index';
import SucessConf from './components/sucessConf/index';
import TransacHisotry from './components/transacHist/index';

const appRoutes = (
  <Route>
    <IndexRoute
      component={Home}
      onEnter={(nextState) =>{
        handleRtChange(nextState, "Paypal UI Exercise", true)}}/>
    <Route
      path="send-money"
      component={SendMoney}
      onEnter={(nextState) =>{
        handleRtChange(nextState, "Send Money")}}/>
    <Route
      path="send-succeeded"
      component={SucessConf}
      onEnter={(nextState)=>{
        handleRtChange(nextState, "Thank you!", true)}}/>
    <Route
      path="transaction-history"
      component={TransacHisotry}
      onEnter={(nextState)=>{handleRtChange(nextState, "Transaction history")}}>
        <Route path="details/:id"/>
    </Route>
    <Route path="*" onEnter={(nextState, replace)=>{replace({pathname: '/'})}}/>
  </Route>
);

function handleRtChange(nextState, nextPageTitle = "", disableBk = false){
  const {
    action,
    pathname,
  } = nextState.location;
  let disableBkBtn = action === "POP" || disableBk;
  appAction.routeChanged(disableBkBtn, nextPageTitle, pathname);
}

export default appRoutes;
