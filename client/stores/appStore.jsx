import Reflux from "reflux";
import appActions from "../actions/appActions";

const appDefaultData = {
  isLoading: false,
  bgDimmed: false,
  previousRoute: null,
  currentRoute: null,
  showBackBtn: false,
  showHomeBtn: false,
  pageTitle: null
}

const appStore = Reflux.createStore({
  init(){
    this.appData = Object.assign({}, appDefaultData);
    this.listenTo(appActions.updateSpinnerState, this.toggleSpinner);
    this.listenTo(appActions.routeChanged, this.handleRouteChange);
  },
  toggleSpinner(loadingState, bgDimmed = false){
    this.appData.isLoading = loadingState;
    this.appData.bgDimmed = bgDimmed;
    this.trigger(this.appData);
  },
  handleRouteChange(disableBk, pageTitle, pathname){
    const {previousRoute, currentRoute, showBackBtn} = this.appData;
    this.appData.previousRoute = this.appData.currentRoute;
    this.appData.currentRoute = pathname;
    this.appData.showBackBtn = this.appData.previousRoute !== "send-succeeded" && !disableBk;
    this.appData.showHomeBtn = this.appData.currentRoute !== "/" && !this.appData.showBackBtn;
    this.appData.pageTitle = pageTitle;
    this.trigger(this.appData);
  },
  resetStore(){
    this.appData = Object.assign({}, appDefaultData);
  },
  getCurrentState(){
    return this.appData;
  },
  getDefaultData(){
    return appDefaultData;
  }
});

export default appStore;
