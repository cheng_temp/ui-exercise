import React, { Component } from "react";

import { PAYMENT_TYPES } from '../../constants'

import BtnGroup from "../common/btnGroup/index";

export default class PaymentTypeSel extends Component {
  _isActive(typeSelected, type){
    return typeSelected === type;
  }
  render() {
    const {
      handleTypeChange,
      typeSelected,
      typeError
    } = this.props;

    const {FRIENDS_FAMILY,GOODS_SERVICES} = PAYMENT_TYPES;

    return (
      <div style={{width: '100%'}}>
        <BtnGroup
          custGrpClassName = "typeSelBtnGrp"
          handleChange = {handleTypeChange}
          count = {2}>
          <BtnGroup.Btn
            displayText = "Send to friends and family"
            iconName = "gift"
            context = {FRIENDS_FAMILY}
            isActive = {this._isActive(typeSelected, FRIENDS_FAMILY)}/>
          <BtnGroup.Btn
            displayText = "Pay for goods and services"
            iconName = "shopping-cart"
            context = {GOODS_SERVICES}
            isActive = {this._isActive(typeSelected, GOODS_SERVICES)}/>
        </BtnGroup>
        {typeError && <p className="formError">{typeError}</p>}
      </div>
    );
  }
}
