import path from 'path';
import express from 'express';
import webpack from 'webpack';
import config from '../webpack.config';

import {apiRoutes} from './api/apiRoutes';

const app = express();
const compiler = webpack(config);
const apiRouter = express.Router();


const WEBPACK_OPTIONS = {
  noInfo: true,
  publicPath: config.output.publicPath
}

app.use(require('webpack-dev-middleware')(compiler, WEBPACK_OPTIONS));
app.use(require('webpack-hot-middleware')(compiler));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../index.html'));
});

app.use('/api', apiRouter);
apiRoutes(apiRouter);

app.listen(8080, 'localhost', (error) => {
  if (error) {
    console.log(error);
    return;
  }
  console.log('Listening at http://localhost:8080');
});
