import React, { Component } from "react";
import {Link, Hisotry} from 'react-router'

import SendOrViewBtns from "../common/sendOrViewBtns/index"

export default class Home extends Component {
  render() {
    return (
      <div>
        <SendOrViewBtns
          handleRouteChange = {(path)=>{
            this.props.history.pushState(null, path);
          }}/>
      </div>
    );
  }
}
