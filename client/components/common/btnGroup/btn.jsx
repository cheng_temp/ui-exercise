import React, { Component } from "react";

class Btn extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      handleChange,
      displayText,
      context,
      iconName,
      isActive,
      colClassName,
      custBtnClassName
     } = this.props;

    let currentContext = context || displayText;

    return (
      <div
        className = {`col-xs-12 ${colClassName} btnContainer`}
        onClick = {handleChange.bind(null, currentContext)}>
        <div className = {`${isActive ? 'btnActive' : ''} ${custBtnClassName || ''} btnBase`}>
          {iconName ? <i className = {`fa fa-${iconName} btnIconBase`}></i> : null}
          <p>{displayText}</p>
        </div>
      </div>
    );
  }
}

export default Btn;
