import React, { Component } from "react";

export default class RecipientEmailInput extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      recipEmail,
      handleEmailChange,
      emailError
    } = this.props;

    return (
      <div>
        <h1 style={{textAlign:"center"}}>To:</h1>
        <input
          className = "form-control"
        	value={recipEmail}
          placeholder="Recipient email address"
          onChange={handleEmailChange}
          ref = {node => {this.recipEmail = node}}/>
        {!!emailError ? <p className="formError">{emailError}</p> : null}
      </div>
    );
  }
}
