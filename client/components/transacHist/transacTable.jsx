import React, { Component } from "react";

import TransacTableRow from './transacTableRow';
import TransacTableRowMobile from './transacTableRowMobile'

export default class TransacHistTable extends Component {
  constructor(props){
    super(props);
  }
  render() {
    const {transactions, isInfinitLoading} = this.props;
    return (
      <div className = "transacHistTable">
        <TransacHistTableBody
          isMobile = {this.props.isMobile}
          transactions = {transactions}/>
      </div>
    );
  }
}


class TransacHistTableBody extends Component {
  constructor(props){
    super(props);
    this._renderTableLine = this._renderTableLine.bind(this);
  }
  _renderTableLine(isMobile){
    return this.props.transactions.map(line => {
      return isMobile ?
        <TransacTableRowMobile key={line.id} data={line}/> :
        <TransacTableRow key={line.id} data={line}/>
    });
  }
  render() {
    return (
      <div style={{width:'100%'}}>
        {this._renderTableLine(this.props.isMobile)}
      </div>
    );
  }
}
