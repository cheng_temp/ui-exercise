import $ from 'jquery';
const TRANSAC_HIST_URL = '/api/transac-hist/';

export function loadTransacHist(accountId, limit = 10, offSet = 50){
  return new Promise((resolve, reject) => {
    const reqUrl = `${TRANSAC_HIST_URL}${accountId}?limit=${limit}&offSet=${offSet}`;
    $.ajax({url:reqUrl}).done((data)=>{
      resolve(data);
    }).fail((failData)=>{
      reject(failData);
    })
  })
}
