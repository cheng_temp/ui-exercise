import React, { Component } from 'react';

import appActions from '../../../actions/appActions';
import appStore from '../../../stores/appStore';

import NavContent from './navContent';

export default class MainNav extends Component {
  constructor(props){
    super(props);
    this.state = appStore.getDefaultData();
    this._onStateChange = this._onStateChange.bind(this)
  }
  componentDidMount() {
    this.unsubscribe = appStore.listen(this._onStateChange);
  }
  componentWillUnmount() {
    this.unsubscribe();
  }
  _onStateChange(data) {
    this.setState(data);
  }
  render() {
    return (
      <div className = 'navContainter'>
        <NavContent {...this.state}
          history={this.props.history}/>
      </div>
    );
  }
}
