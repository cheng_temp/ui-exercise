##Running the app:

>`npm install`
>`npm start`
> App should be running on http://localhost:8080/

#### Stack / Libraries used
> ReactJS
> React-router
> Reflux
> Bootstrap (Basic styling only, layout is done with flex-box)
> Express
> Webpack
> Babel for ES6 transpiling

### TODOs:
> Refactor `SendMoney` component to connect with Reflux store (Currently designed as a dumb component)
> SCSS cleanup and refactor
> Tests :)
