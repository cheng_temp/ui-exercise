import Reflux from 'reflux';
import transacHistActions from '../actions/transacHistActions';

const transacHistDefaultData = {
  limit: 20,
  offset: 0,
  isInitLoading: true,
  isInfinitLoading: false,
  transactions: []
}

const transacHistStore = Reflux.createStore({
  init(){
    this.transacHistData = Object.assign({}, transacHistDefaultData);
    this.listenTo(transacHistActions.loadHist, this.toggleInfinitLoad);
    this.listenTo(transacHistActions.loadHist.completed, this.updateTransac);
  },
  toggleInfinitLoad(){
    this.transacHistData.isInfinitLoading = true;
    this.trigger(this.transacHistData);
  },
  updateTransac(data){
    let {transactions} = this.transacHistData;
    this.transacHistData.isInitLoading = false;
    this.transacHistData.isInfinitLoading = false;
    this.transacHistData.offset += data.transactions.length;
    this.transacHistData.transactions = [...transactions, ...data.transactions];
    this.trigger(this.transacHistData);
  },
  resetStore(){
    this.transacHistData = Object.assign({}, transacHistDefaultData);
  },
  getCurrentState(){
    return this.transacHistData;
  },
  getDefaultData(){
    return transacHistDefaultData;
  }
});

export default transacHistStore;
