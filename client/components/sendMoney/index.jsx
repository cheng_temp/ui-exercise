import React, { Component } from "react";

import { validateEmail } from '../../utils/validations';
import { CURRENCY_MAP } from '../../constants'
import appActions from "../../actions/appActions";


import AmountInput from './sendAmount/amountInput'
import CurrencySel from './sendAmount/currencySel'
import RecipEmailInput from './recipEmailInput'
import PaymentTypeSel from './paymentTypeSel'
import SubmitPaymentBtn from './submitPaymentBtn'

const EMAIL_ERR_MSG = "A valid email address is required";
const AMOUNT_ERR_MSG = "Amount can not be zero";
const TYPE_ERR_MSG = "Type is required";



export default class SendMoneyForm extends Component {
  constructor(props) {
    super(props);
    this._handleEmailChange = this._handleEmailChange.bind(this);
    this._handleAmountChange = this._handleAmountChange.bind(this);
    this._handleCurrencyChange = this._handleCurrencyChange.bind(this);
    this._handleTypeChange = this._handleTypeChange.bind(this);
    this._validateForm = this._validateForm.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
    this.defaultState = {
      recipEmail: null,
      sendAmount: null,
      typeSelected: null,
      currency: "USD",
      avilCurrency: ["USD", "EUR", "JPY"],
      emailError: false,
      amountError: false,
      typeError: false,
      clearAll: false
    }
    this.state = Object.assign({}, this.defaultState);
  }
  _handleEmailChange(event) {
    let {value} = event.target;
    const errorMsg = validateEmail(value) ? false : EMAIL_ERR_MSG;
    this.setState({
      recipEmail: value,
      emailError: errorMsg
    });
  }
  _handleAmountChange(event, val = false) {
    const inputVal = val || event.target.value;
    const input = inputVal.replace(/\D/g,'');
    if(input === '') return;
    const numStr = `${input.substring(0, input.length -2)}.${input.slice(-2)}`;
    const numObj = new Number(parseFloat(numStr).toFixed(2));
    const LOCALE = CURRENCY_MAP[this.state.currency].LOC;
    const newAmount = numObj == 0.00 ? "0.00" : numObj.toLocaleString(LOCALE);
    this.setState({sendAmount: newAmount});
  }
  _handleCurrencyChange(event) {
    const input =  event.target.value;
    this.setState({currency: input}, ()=>{
      this._handleAmountChange(null, this.state.sendAmount);
    });
  }
  _handleTypeChange(type){
    this.setState({
      typeSelected: type
    });
  }
  _validateForm(){
    const {sendAmount, recipEmail, emailError, typeSelected} = this.state;
    let hasAmount = sendAmount !== "0.00" && sendAmount !== null;
    let hasValidateEmail = recipEmail !== null && !emailError;
    let hasType = typeSelected !== null;

    if(!hasType || !hasValidateEmail || !hasAmount){
      const newErrorState = {
        amountError: hasAmount ? false : AMOUNT_ERR_MSG,
        typeError: hasType ? false : TYPE_ERR_MSG,
        emailError: hasValidateEmail ? false : EMAIL_ERR_MSG,
      };
      this.setState(newErrorState);
      return false;
    }
    return true;
  }
  _handleSubmit(isReset = false){
    if(isReset){
      return this.setState(this.defaultState);
    }
    if(!this._validateForm()) return false;
    appActions.updateSpinnerState(true, true);
    setTimeout(()=>{
      this.props.history.pushState(null, 'send-succeeded');
    }, 700);
  }
  render() {
    const {
      state,
      _handleEmailChange,
      _handleTypeChange,
      _handleAmountChange,
      _handleCurrencyChange,
      _handleSubmit
    } = this;

    return (
      <div className = "sendMoneyComp">
        <RecipEmailInput {...state}
          handleEmailChange = {_handleEmailChange}/>
        <AmountInput {...state}
          handleAmountChange = {_handleAmountChange}/>
        <CurrencySel {...state}
          handleCurrencyChange = {_handleCurrencyChange}/>
        <PaymentTypeSel {...state}
          handleTypeChange = {_handleTypeChange}/>
        <SubmitPaymentBtn {...state}
          handleSubmit = {_handleSubmit}/>
      </div>
    );
  }
}
