import fs from 'fs';
import path from 'path';

import FakeDataStore from '../utils/FakeDataStore';
const FakeData = new FakeDataStore(300);

export function fetchTransacHist(req, res, next){
  const {accountId} = req.params;
  const {offSet, limit} = req.query;

  const transacHistRecords = FakeData.getRecordsByRange(offSet,limit);
  if(!!transacHistRecords){
    setTimeout(()=>{res.json({transactions : transacHistRecords})}, 500);
  } else {
    res.status(404).send("Not found");
  }
}


export function fetchStaticTransacHist(req, res, next){
  const {accountId} = req.params;
  const {offSet, limit} = req.query;
  const dataSourcePath = path.join(__dirname, '../devData/transactions.json');

  fs.readFile(dataSourcePath, (error, data) => {
    if(error) return res.status(404).send("Not found");
    let record = JSON.parse(data);
    if(offSet > record.transactions.length){
      return res.status(404).send("Not found")
    }
    res.json(transactions.slice(offSet, parseInt(offSet) + parseInt(limit)));
  });
}


export function fetchTransacDetails(req, res, next){
  let {transId} = req.params;
  const transacRecord = FakeData.getSingleRecord(transId);
  if(!!transacRecord){
    res.json({transaction : transacRecord});
  } else {
    res.status(404).send("Not found");
  }
}
