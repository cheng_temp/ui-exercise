import React, { Component } from "react";

export default class SubmitPaymentBtn extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      submitDisabled,
      handleSubmit
    } = this.props;
    return (
      <div>
        <button
          className={`btn btn-primary btn-lg ${submitDisabled ? 'disabled' : ''}`}
          onClick={handleSubmit.bind(null, false)}>
          Submit payment
        </button>
        <button
          className={`btn btn-danger btn-lg ${submitDisabled ? 'disabled' : ''} sendMoneyFormBtns`}
          onClick={handleSubmit.bind(null, true)}>
          Clear
        </button>
      </div>
    );
  }
}
